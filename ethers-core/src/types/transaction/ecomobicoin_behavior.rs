use super::{eip2718::TypedTransaction, normalize_v, rlp_opt};
use crate::types::{Bytes, Signature, Transaction, TransactionRequest, U256, U64};
use rlp::{Decodable, RlpStream};
use serde::{Deserialize, Serialize};
use thiserror::Error;

const NUM_BX_FIELDS: usize = 4;

/// An error involving an EcoMobiCoinBehavior transaction request.
#[derive(Debug, Error)]
pub enum EcoMobiCoinBehaviorRequestError {
    /// When decoding a transaction request from RLP
    #[error(transparent)]
    DecodingError(#[from] rlp::DecoderError),
}

/// Parameters for sending a transaction
#[derive(Clone, Default, Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct EcoMobiCoinBehaviorTransactionRequest {
    #[serde(flatten)]
    pub tx: TransactionRequest,

    // TODO Use nonce as timestamp ?
    #[serde(skip_serializing_if = "Option::is_none")]
    pub timestamp: Option<U64>,
}

impl EcoMobiCoinBehaviorTransactionRequest {
    pub fn new(
        tx: TransactionRequest,
        // quantity: Option<U256>, // using tx.value ATM
        // could use tx.nonce for timestamp
        timestamp: Option<U64>,
        // input: Option<Bytes>, // duplicate with tx.data
    ) -> Self {
        Self { tx, timestamp }
    }

    pub fn rlp(&self) -> Bytes {
        let mut rlp = RlpStream::new();
        rlp.begin_list(NUM_BX_FIELDS);
        self.rlp_base(&mut rlp);
        rlp.out().freeze().into()
    }

    /// Produces the RLP encoding of the transaction with the provided signature
    pub fn rlp_signed(&self, signature: &Signature) -> Bytes {
        let mut rlp = RlpStream::new();
        rlp.begin_unbounded_list();
        self.rlp_base(&mut rlp);

        rlp.append(&signature.v);
        rlp.append(&signature.r);
        rlp.append(&signature.s);
        rlp.finalize_unbounded_list();
        rlp.out().freeze().into()
    }
    pub(crate) fn rlp_base(&self, rlp: &mut RlpStream) {
        rlp_opt(rlp, &self.tx.chain_id);
        // rlp_opt(&mut rlp, &self.tx.to);
        rlp_opt(rlp, &self.tx.value);
        rlp_opt(rlp, &self.timestamp);
        rlp_opt(rlp, &self.tx.data.as_ref().map(|d| d.as_ref()));
        // encode also happens in response.rs :)
    }

    /// Decodes fields based on the RLP offset passed.
    fn decode_base_rlp(rlp: &rlp::Rlp, offset: &mut usize) -> Result<Self, rlp::DecoderError> {
        let mut tx = TransactionRequest::new();
        tx.chain_id = Some(rlp.val_at(*offset)?);
        *offset += 1;
        tx.value = Some(rlp.val_at(*offset)?);
        *offset += 1;
        let timestamp: Option<U64> = Some(rlp.val_at(*offset)?);
        *offset += 1;
        let data = rlp::Rlp::new(rlp.at(*offset)?.as_raw()).data()?;
        tx.data = match data.len() {
            0 => None,
            _ => Some(Bytes::from(data.to_vec())),
        };
        *offset += 1;

        Ok(Self { tx, timestamp })
    }

    /// Decodes the given RLP into a transaction
    /// Note: this transaction does not have a signature
    pub fn decode_signed_rlp(
        rlp: &rlp::Rlp,
    ) -> Result<(Self, Signature), EcoMobiCoinBehaviorRequestError> {
        let mut offset = 0;
        let mut txn = Self::decode_base_rlp(rlp, &mut offset)?;
        let v = rlp.val_at(offset)?;
        offset += 1;
        let r = rlp.val_at(offset)?;
        offset += 1;
        let s = rlp.val_at(offset)?;

        let sig = Signature { r, s, v };
        txn.tx.from = Some(
            sig.recover(TypedTransaction::EcoMobiCoinBehavior(txn.clone()).sighash()).unwrap(),
        );
        Ok((txn, sig))
    }
}

/// Get a EcoMobiCoinBehaviorTransactionRequest from a rlp encoded byte stream
impl Decodable for EcoMobiCoinBehaviorTransactionRequest {
    fn decode(rlp: &rlp::Rlp) -> Result<Self, rlp::DecoderError> {
        Self::decode_base_rlp(rlp, &mut 0)
    }
}

/// Get an EcoMobiCoinBehavior transaction request from a Transaction
impl From<&Transaction> for EcoMobiCoinBehaviorTransactionRequest {
    fn from(tx: &Transaction) -> EcoMobiCoinBehaviorTransactionRequest {
        EcoMobiCoinBehaviorTransactionRequest { tx: tx.into(), timestamp: tx.timestamp }
    }
}

#[cfg(feature = "ecomobicoin")]
#[cfg(not(feature = "celo"))]
#[cfg(test)]
mod test {

    use crate::types::{
        transaction::{
            ecomobicoin_behavior::EcoMobiCoinBehaviorTransactionRequest, eip2718::TypedTransaction,
        },
        Address, Bytes, NameOrAddress, Transaction, TransactionRequest, H256, U256, U64,
    };
    use rlp::Decodable;
    use std::str::FromStr;

    #[test]
    fn test_rlp_encode_ecomobicoin_behavior_tx() {
        let bx = Transaction {
            // hash: H256::from_str("0x7fd17d4a368fccdba4291ab121e48c96329b7dc3d027a373643fb23c20a19a3f").unwrap(),
            hash: H256::zero(),
            nonce: U256::zero(),
            block_hash: Some(H256::from_str("0xc2794a16acacd9f7670379ffd12b6968ff98e2a602f57d7d1f880220aa5a4973").unwrap()),
            block_number: Some(8453214u64.into()),
            transaction_index: Some(0u64.into()),
            from: Address::from_str("0xdeaddeaddeaddeaddeaddeaddeaddeaddead0001").unwrap(),
            to: Some(Address::from_str("0x4200000000000000000000000000000000000015").unwrap()),
            value: U256::from(4391989),
            gas_price: Some(U256::zero()),
            gas: U256::zero(),
            input: Bytes::from(
                hex::decode("015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240").unwrap()
            ),
            v: U64::zero(),
            r: U256::zero(),
            s: U256::zero(),
            timestamp: Some(U64::one()),
            transaction_type: Some(U64::from(0x3E)),
            access_list: None,
            max_priority_fee_per_gas: None,
            max_fee_per_gas: None,
            chain_id: Some(U256::from(63)),
        };

        let rlp = bx.rlp();

        let expected_rlp = Bytes::from(hex::decode("3ef901103f8343043501b90104015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240808080").unwrap());

        assert_eq!(rlp, expected_rlp);
    }

    #[test]
    fn test_rlp_decode_ecomobicoin_behavior_tx() {
        let encoded = Bytes::from(hex::decode("3ef901103f8343043501b90104015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240808080").unwrap());
        let tx = TypedTransaction::decode(&rlp::Rlp::new(&encoded)).unwrap();

        assert!(matches!(tx, TypedTransaction::EcoMobiCoinBehavior(_)));

        assert_eq!(tx.gas(), None);
        assert_eq!(tx.gas_price(), None);
        assert_eq!(tx.value(), Some(&U256::from(4391989)));
        assert_eq!(tx.nonce(), None);
        assert_eq!(tx.chain_id(), Some(U64::from(63)));
        // assert_eq!(
        //     tx.to(),
        //     Some(&NameOrAddress::Address(
        //         Address::from_str("0x4200000000000000000000000000000000000015").unwrap()
        //     ))
        // );
    }

    #[test]
    fn test_rlp_encode_bx() {
        let bx = EcoMobiCoinBehaviorTransactionRequest {
            tx: TransactionRequest {
                // hash: H256::from_str("
                // 0x7fd17d4a368fccdba4291ab121e48c96329b7dc3d027a373643fb23c20a19a3f").unwrap(),
                nonce: Some(U256::from(4391989)),
                from: Some(Address::from_str("0xdeaddeaddeaddeaddeaddeaddeaddeaddead0001").unwrap()),
                to: Some(NameOrAddress::Address(Address::from_str("0x4200000000000000000000000000000000000015").unwrap())),
                value: Some(U256::from(4391989)),
                gas_price: Some(U256::zero()),
                gas: Some(U256::zero()),
                chain_id: Some(U64::from(63)),
                data: Some(Bytes::from(
                    hex::decode("015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240").unwrap()
                )),
            },
            timestamp: Some(U64::one()),
        };

        let rlp: Bytes = bx.rlp();

        // Not same RLP encoding from transaction and transaction request
        let expected_rlp = Bytes::from(hex::decode("f9010d3f8343043501b90104015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240").unwrap());

        assert_eq!(rlp, expected_rlp);
    }

    #[test]
    fn test_rlp_encode_decode_ecomobicoin_behavior() {
        let bx = Transaction {
            hash: H256::from_str("0x7fd17d4a368fccdba4291ab121e48c96329b7dc3d027a373643fb23c20a19a3f").unwrap(),
            // hash: H256::zero(),
            nonce: U256::from(4391989),
            block_hash: Some(H256::from_str("0xc2794a16acacd9f7670379ffd12b6968ff98e2a602f57d7d1f880220aa5a4973").unwrap()),
            block_number: Some(8453214u64.into()),
            transaction_index: Some(0u64.into()),
            from: Address::from_str("0xdeaddeaddeaddeaddeaddeaddeaddeaddead0001").unwrap(),
            to: Some(Address::from_str("0xdeaddeaddeaddeaddeaddeaddeaddeaddead0001").unwrap()),
            value: U256::from(1337),
            gas_price: Some(U256::zero()),
            gas: U256::from(1000000u64),
            input: Bytes::from(
                hex::decode("015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f4240").unwrap()
            ),
            v: U64::one(),
            r: U256::one(),
            s: U256::one(),
            timestamp: Some(U64::from(1687964647)),
            transaction_type: Some(U64::from(0x3E)),
            access_list: None,
            max_priority_fee_per_gas: None,
            max_fee_per_gas: None,
            chain_id: Some(U256::from(63)),
        };

        // Encode
        let rlp = bx.rlp();
        println!("rlp: {:?}", hex::encode(rlp.clone()));
        // Decode
        let tx = TypedTransaction::decode(&rlp::Rlp::new(&rlp)).unwrap();

        assert!(matches!(tx, TypedTransaction::EcoMobiCoinBehavior(_)));
        assert_eq!(tx.gas(), None);
        assert_eq!(tx.gas_price(), None);
        assert_eq!(tx.value(), Some(&U256::from(1337)));
        assert_eq!(tx.nonce(), None);
        assert_eq!(tx.chain_id(), Some(U64::from(63)));
        // assert_eq!(tx.timestamp(), Some(U64::from(1687964647)));
        // assert_eq!(
        //     tx.to(),
        //     Some(&NameOrAddress::Address(
        //         Address::from_str("0xdeaddeaddeaddeaddeaddeaddeaddeaddead0001").unwrap()
        //     ))
        // );
    }

    #[ignore]
    #[test]
    fn test_rlp_decode_ecomobicoin_behavior_tx_truite() {
        // UNSIGNED
        let encoded = Bytes::from(
            hex::decode("3ef901553f84649d9cd1c483033450b90104015d8eb90000000000000000000000000000000000000000000000000000000000878c1c00000000000000000000000000000000000000000000000000000000644662bc0000000000000000000000000000000000000000000000000000001ee24fba17b7e19cc10812911dfa8a438e0a81a9933f843aa5b528899b8d9e221b649ae0df00000000000000000000000000000000000000000000000000000000000000060000000000000000000000007431310e026b69bfc676c0013e12a1a11411eec9000000000000000000000000000000000000000000000000000000000000083400000000000000000000000000000000000000000000000000000000000f424001a0260c750d40e5b52347eeb0d1772fbe0f77602ae346dbfaf1e1cada02ea3394fba02731e8a886a4e0e881be66bb3ae3fc87f0e6635b23ffa94c6273cb2e87670654")
                .unwrap(),
        );
        let (tx, signature) = TypedTransaction::decode_signed(&rlp::Rlp::new(&encoded)).unwrap();

        // problem with address encoding on the tx.to field that seems to be None rather than
        // Some()?

        assert!(matches!(tx, TypedTransaction::EcoMobiCoinBehavior(_)));

        assert!(signature.recovery_id().is_ok());

        assert_eq!(tx.gas(), None);
        assert_eq!(tx.gas_price(), None);
        assert_eq!(tx.value(), Some(&U256::from(4391989)));
        assert_eq!(tx.nonce(), None);
        assert_eq!(tx.chain_id(), Some(U64::from(63)));
    }
}
